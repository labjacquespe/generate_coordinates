Generate coordinates tool : VAP Utils
=====================================
Copyright (C) 2014-2017 Charles Coulombe
Université de Sherbrooke. All rights reserved.

Run this tool to generate a reference coordinates file (rc[1-6]) from a [genePred](https://genome.ucsc.edu/FAQ/FAQformat#format9) reference annotations file.

Usage
-----
```
Options:
  -h [ --help ]                   show help message
  -v [ --version ]                show version
  -n [ --number-coordinates ] arg Number of coordinates.
  -i [ --input-file ] arg         Source file.
  -o [ --output-file ] arg        Ouptud file.
  -a [ --alias ] arg              Ouptud file alias.
  -f [ --filter-file ] arg        A file containing a list of annotations to generate. By default, all annotations are processed. Empty or no file means all annotations.
  -5 [ --5prime ]                 One reference point is aligned on 5 prime boundary.
  -3 [ --3prime ]                 One reference point is aligned on 3 prime boundary.
```

Run
---
To create an output file `myfile.rc4` with the alias `myalias`, of format `rc4` from `input.genepred`, run the following :

```
./generate_coordinates --ncurves 4 --input-file input.genepred --output-file myfile --filter-file filters.txt --alias myalias
```

To create an output file `myfile` of format `coord1` 3-prime from `input.genepred`, run the following :

```
./generate_coordinates --ncurves 1 --input-file input.genepred --output-file myfile --3prime
```

Build
-----
The tool require the Boost library _program options_ to be built.

Run make utility in the tool directory.

```
make
```

Bug Reports and Feedback
------------------------
You can report a bug [here](https://bitbucket.org/labjacquespe/generate_coordinates/issues).
You can also directly contact us via email by writing to vap_support at usherbrooke dot ca.

Any and all feedback is welcome.

Licensing
---------
VAP is released under the [GPL license](http://www.gnu.org/licenses/gpl-3.0.txt). For further details see the COPYING file
in this directory.
