# Charles Coulombe
# Version 0.0.1 compatible

NAME=generate_coordinates

CXXFLAGS=-std=c++11

LXXFLAGS=-lboost_program_options

.PHONY: all clean distclean

all:
	g++ -O3 $(CXXFLAGS) $(NAME).cpp $(LXXFLAGS) -o $(NAME)

# Clean
clean:
	$(RM) $(NAME)

distclean: clean
