/**
 * @file    generate_coordinates.cpp
 * @author  Charles Coulombe
 * @date    28 November 2014, 09:21
 * @version 1.0.0
 *
 * @copyright
 * Copyright (C) 2014-2015 Charles Coulombe
 * Université de Sherbrooke. All rights reserved.
 *
 * vap_utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * vap_utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if __cplusplus < 201103L
    #error A C++11 compliant compiler is required.
#endif

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <deque>
#include <map>
#include <set>
#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

#define APP_NAME "generate_coordinates"
#define APP_VERSION "0.1"

inline char strand(char c)
{
    if(c == '-') return '-';
    else         return '+';
}

/** Command line arguments */
struct parameters
{
    /** Number of coordinates */
    unsigned int number;

    /** Source file. */
    std::string inputfile;

    /** Result file */
    std::string outputfile;

    /** File alias */
    std::string alias;

    /** Is 1 refpt 5-prime or 3-prime. */
    bool fiveprime;

    /** File containing a list of annotations to use. Can be empty to mean all. */
    std::string filterfile;

    /**
      * Construct parameters with values.
      *
      * @param n Number of coordinates
      * @param in Source file
      * @param out Result file
      */
    parameters(const unsigned int n = 0, const std::string &in = "", const std::string &out = "", const std::string &a = "", bool fp = true, const std::string &ifrom = "")
        : number(n), inputfile(in), outputfile(out), alias(a), fiveprime(fp), filterfile(ifrom) {}
};

/** Genome entry */
struct genome_entry
{
    std::string chromosome;
    char strand;
    int begin;
    int end;
    std::string name;

    genome_entry(const std::string &c = "", char s = '\0', const int b = 0, const int e = 0, const std::string n = "")
        : chromosome(c), strand(s), begin(b), end(e), name(n) { }
};

/** Genome entry pointers */
struct genome_entry_ptr
{
    const genome_entry *prior; // prior genome entry
    const genome_entry *main;  // main genome entry
    const genome_entry *next;  // next genome entry

    genome_entry_ptr(const genome_entry *pr, const genome_entry *mn, const genome_entry *nt)
        : prior(pr), main(mn), next(nt) { }
};

namespace internal
{
/** dataset key ordering object */
struct _less_dataset
{

    bool operator()(const std::string &l, const std::string &r) const
    {
        return l < r;
    }
};

inline bool genome_entry_compare(const genome_entry &left, const genome_entry &right)
{
    // chromosome : true when left chromosome comes before right chromosome, false otherwise
    // begin : true when left begin point comes before right begin point, false otherwise
    // end : true when right end point comes before left end point
    // ordering : chromosome(asc), begin(asc), end(desc)

    if(left.chromosome < right.chromosome)
        return true;

    if(left.chromosome == right.chromosome)
    {
        if(left.begin < right.begin)
            return true;

        else if(left.begin == right.begin && left.end > right.end)
            return true;
    }

    return false;
}
} // end of internal namespace

typedef std::map<std::string, std::deque<genome_entry>, internal::_less_dataset> genome_dataset;

// Workaround: Boost PO has a bug that does not display option value, so do it for the library
template <class T>
std::string invalid_option_value_str(const std::string &option_name, const T &option_value, const std::string &context)
{
    std::string msg = "the argument '" + boost::lexical_cast<std::string>(option_value) + "' for the option '" + option_name + "' is invalid";

    if(context.empty())
        return msg;
    else
        return msg.append(" (").append(context).append(")");
}

template <class T>
std::string conflicting_option_value_str(const std::string &option_name1, const std::string &option_name2 , const T &option_value1, const T &option_value2, const std::string &context)
{
    std::string msg = "the arguments '" + boost::lexical_cast<std::string>(option_value1) + "' and '" + boost::lexical_cast<std::string>(option_value2) + "' for the options '" + option_name1 + "' and '" + option_name2 + "' are conflicting";

    if(context.empty())
        return msg;
    else
        return msg.append(" (").append(context).append(")");
}

/**
 * Checks if given file exists. Path must be well structured.
 *
 * @param path file's path
 * @return @c True if file exists and can be open, @c False otherwise
 */
inline bool fileExists(const std::string &path)
{
    std::ifstream ifs(path.c_str());
    return ifs.is_open();
}

/**
 * Parses arguments from command prompt or exits if no parameter file was provided.
 *
 * @param ac number of arguments
 * @param av array of arguments
 * @return parameters
 */
parameters parseArguments(int ac, char const *av[])
{
    namespace po = boost::program_options;

    parameters params;

    po::variables_map vm;
    po::options_description cmd_options;

    po::options_description options("Options");
    options.add_options()
    ("help,h", "show help message")
    ("version,v", "show version")
    ("number-coordinates,n", po::value<std::string>()->required(), "Number of coordinates.")
    ("input-file,i", po::value<std::string>()->required(), "Source file.")
    ("output-file,o", po::value<std::string>()->required(), "Output file.")
    ("alias,a", po::value<std::string>(), "Output file alias.")
    ("filter-file,f", po::value<std::string>(), "A file containing a list of annotations to generate. By default, all annotations are processed. Empty or no file means all annotations.")
    ("5prime,5", po::bool_switch()->default_value(false), "One reference point is aligned on 5 prime boundary.")
    ("3prime,3", po::bool_switch()->default_value(false), "One reference point is aligned on 3 prime boundary.")
    ;

    // Parse command line options first.
    try
    {
        store(po::command_line_parser(ac, av).options(options).run(), vm);

        if(vm.count("help"))
        {
            std::cout << APP_NAME << " " << APP_VERSION << "\n"
                      << "Usage: " << APP_NAME << " OPTIONS... FILE" << "\n\n"
                      << std::boolalpha << options << "\n\n"
                      << "Examples:\n"
                      << "generate_coordinates --ncurves 4 --input-file input.genepred --output-file myfile --filter-file filters.txt --alias myalias\n"
                      << "generate_coordinates --ncurves 1 --input-file input.genepred --output-file myfile --3prime\n"
                      << "\n"
                      << "Report bugs to: <https://bitbucket.org/labjacquespe/generate_coordinates/issues>\n";
            std::exit(EXIT_SUCCESS);
        }

        if(vm.count("version"))
        {
            std::cout << APP_NAME << " " << APP_VERSION << "\n"
                      << "License GPLv3: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>." << '\n';
            std::exit(EXIT_SUCCESS);
        }

        notify(vm);

        if(vm.count("number-coordinates"))
        {
            std::vector<char> v = {'1', '2', '3', '4', '5', '6'};
            std::string value = vm["number-coordinates"].as<std::string>();

            // Make sure value is between 1 and 6.
            if(value.empty() || value.size() != 1 || std::none_of(v.begin(), v.end(), [&value](char c) { return c == value.front(); }))
            throw std::runtime_error(invalid_option_value_str("number-coordinates", value, "must be a number between 1 and 6"));

            params.number =  boost::lexical_cast<unsigned int>(value);
        }

        if(vm.count("input-file"))
        {
            std::string value = vm["input-file"].as<std::string>();

            // Make sure the file exists
            if(value.empty() || !fileExists(value))
                throw std::runtime_error(invalid_option_value_str("input-file", value, "no such file"));

            params.inputfile = value;
        }

        if(vm.count("output-file"))
        {
            const std::string extension = ".rc";

            params.outputfile = vm["output-file"].as<std::string>() + extension + std::to_string(params.number);
        }

        if(vm.count("alias"))
        {
            params.alias = vm["alias"].as<std::string>();
        }

        if(vm.count("filter-file"))
        {
            std::string value = vm["filter-file"].as<std::string>();

            // Make sure the file exists
            if(value.empty() || !fileExists(value))
                throw std::runtime_error(invalid_option_value_str("filter-file", value, "no such file"));

            params.filterfile = value;
        }

        if(vm.count("5prime") || vm.count("3prime"))
        {
            bool fiveprime = vm["5prime"].as<bool>();
            bool threeprime = vm["3prime"].as<bool>();

            if(fiveprime && threeprime || params.number == 1 && !fiveprime && !threeprime)
                throw std::runtime_error(conflicting_option_value_str("5prime", "3prime", fiveprime, threeprime, "cannot be both"));
            /**
             * 5prime true, 3prime false -> true
             * 5prime false, 3prime true -> false
             */
            params.fiveprime = fiveprime;
        }
    }
    catch(std::exception &e)
    {
        std::cerr << "error: " << e.what() << "\n";
        std::exit(1);
    }

    return params;
}

void addDummiesEntry(genome_dataset &dataset)
{
    // define iterator type
    typedef genome_dataset::iterator it_t;

    // TRICKY PART :
    //
    // For each chromosome, add two dummy entries, one at the front and the other at the back.
    //
    // front dummy entry :
    //      Must be Watson oriented so there's no promoters in front and its coordinates must be reversed so that
    //      begin point is the same as the real annotation that follows, and the end point is 1 bp less.
    //
    // back dummy entry :
    //      Must be Crick oriented so there's no terminator after and its coordinates must be reversed so that
    //      end point is the same as the real annotation that follows, and the end point is 1 bp less.
    //
    // To simulate the above scenario, the values are hard coded.
    for(it_t it = dataset.begin(); it != dataset.end(); ++it)
    {
        // Dummy coordinates are used as half-open coordinates to generate closed zero-based coordinate to match VAP internal representation.
        it->second.insert(it->second.begin(), genome_entry("chr", '+', 0, 0,              "dummy_begin_chr"));
        it->second.push_back(genome_entry("chr", '-', 99999999, 99999999, "dummy_end_chr"));
    }
}

void readFilter(const std::string &file, std::set<std::string> &filter)
{
    std::ifstream in(file);
    std::string name = "";

    while(std::getline(in, name))
    {
        if(name.empty() || name[0] == '#')
            continue;

        filter.insert(name);
    }
}

void readGenome(const std::string &file, genome_dataset &genome)
{
    std::ifstream in(file);

    std::istringstream iss;
    std::string line = "";
    std::string chromosome = "";
    std::string name = "";
    char strand = '\0';
    int begin = 0;
    int end = 0;

    line.reserve(5000);
    chromosome.reserve(100);
    name.reserve(100);

    while(std::getline(in, line))
    {
        iss.str(line);
        iss >> name >> chromosome >> strand >> begin >> end;

        if(iss.fail())
        {
            iss.clear();
            continue;
        }

        genome[chromosome].push_back(genome_entry(chromosome, strand, begin, end, name));
    }

    // for each chromosome, sort their entries, chromosomes are already
    // ordered(asc) by their insertion in the container(map)
    // it_t(defined previsouly speaks for itself) points to a node in the container(map)
    // and associated value(second) are entries contained in a vector
    for(genome_dataset::iterator i = genome.begin(); i != genome.end(); ++i)
        std::sort(i->second.begin(), i->second.end(), internal::genome_entry_compare);

    addDummiesEntry(genome);
}

void write(const std::string &file, const std::vector<genome_entry_ptr> &genome_ptrs, const parameters &params, const std::set<std::string> &filter)
{
    std::ofstream out(file, std::ios::trunc);
    std::vector<int> coordinates;

    // write header
    if(params.number == 1)
        out << "#name=\"" << params.alias << "\" type=\"coord" << params.number << "\" desc=\"" << params.number << " reference points coordinates " << (params.fiveprime ? "5 prime" : "3 prime") << "\"" << "\n";
    else
        out << "#name=\"" << params.alias << "\" type=\"coord" << params.number << "\" desc=\"" << params.number << " reference points coordinates\"" << "\n";

    // write data
    for(std::vector<genome_entry_ptr>::const_iterator i = genome_ptrs.begin(); i != genome_ptrs.end(); ++i)
    {
        // if annotation's name is present or no filter, write annotation
        if(filter.count(i->main->name) > 0 || filter.empty())
        {
            coordinates.clear();

            // push the correct coordinates regarding the number of reference points
            switch(params.number)
            {
                case 1:
                    // Watson : Take start point.
                    // Crick : Take end point.

                    // In annotation mode, the 5 prime is associated to the start of the second block while the 3 prime is assosicated to
                    // the end of the first block. In coordinates mode, only the 5 prime is considered. Therefore, we adjust the
                    // coordinate in 3 prime to obtain the same results as in annotations mode.

                    if(strand(i->main->strand) == '-')
                        coordinates.push_back(params.fiveprime ? (i->main->end - 1) : (i->main->begin - 1)); // crick annotation of interest
                    else
                        // Simulate
                        coordinates.push_back(params.fiveprime ? (i->main->begin) : (i->main->end));   // watson annotation of interest
                    break;
                case 2:
                    // Keep the same coordinates value.
                    coordinates.push_back(i->main->begin);   // annotation of interest
                    coordinates.push_back(i->main->end - 1); // annotation of interest
                    break;
                case 3:
                    if(strand(i->main->strand) == '-')
                    {
                        coordinates.push_back(i->main->begin);   // annotation of interest
                        coordinates.push_back(i->main->end - 1); // annotation of interest
                        coordinates.push_back(i->next->begin);   // next annotation
                    }
                    else
                    {
                        coordinates.push_back(i->prior->end - 1); // prior annotation
                        coordinates.push_back(i->main->begin);   // annotation of interest
                        coordinates.push_back(i->main->end - 1); // annotation of interest
                    }
                    break;
                case 4:
                    coordinates.push_back(i->prior->end - 1);  // prior annotation
                    coordinates.push_back(i->main->begin);   // annotation of interest
                    coordinates.push_back(i->main->end - 1);   // annotation of interest
                    coordinates.push_back(i->next->begin);   // next annotation
                    break;
                case 5:
                    if(strand(i->main->strand) == '-')
                    {
                        coordinates.push_back(i->prior->end - 1);  // prior annotation
                        coordinates.push_back(i->main->begin);   // annotation of interest
                        coordinates.push_back(i->main->end - 1);   // annotation of interest
                        coordinates.push_back(i->next->begin);   // next annotation
                        coordinates.push_back(i->next->end - 1); // next annotation
                    }
                    else
                    {
                        coordinates.push_back(i->prior->begin);  // prior annotation
                        coordinates.push_back(i->prior->end - 1);  // prior annotation
                        coordinates.push_back(i->main->begin);   // annotation of interest
                        coordinates.push_back(i->main->end - 1);   // annotation of interest
                        coordinates.push_back(i->next->begin);   // next annotation
                    }
                    break;
                case 6:
                    coordinates.push_back(i->prior->begin);  // prior annotation
                    coordinates.push_back(i->prior->end - 1);  // prior annotation
                    coordinates.push_back(i->main->begin);   // annotation of interest
                    coordinates.push_back(i->main->end - 1);   // annotation of interest
                    coordinates.push_back(i->next->begin);   // next annotation
                    coordinates.push_back(i->next->end - 1);   // next annotation
                    break;
                default:
                    break;
            }

            // print chromosome and strand
            out << i->main->chromosome << '\t' << i->main->strand << '\t';

            // print all coordinates
            for(std::vector<int>::iterator j = coordinates.begin(); j != coordinates.end(); ++j)
                out << *j << '\t';

            // finally print the name
            out << i->main->name << "\n";
        }
    }
}

void buildGenomePtrs(std::vector<genome_entry_ptr> &genomePtrs, genome_dataset &dataset)
{
    unsigned int memory = 0;

    // pre-process chromosomes to calculate total memory amount required
    for(genome_dataset::const_iterator i = dataset.begin(); i != dataset.end(); ++i)
        memory += i->second.size();

    // reserve just enough memory for all chr among the different groups, hence avoiding copy of element on growth
    genomePtrs.reserve(memory);

    // insert genome pointers, loop for all chromosomes
    for(genome_dataset::const_iterator i = dataset.begin(); i != dataset.end(); ++i)
    {
        // loop for all entries of the current chromosome
        for(std::deque<genome_entry>::const_iterator j = i->second.begin() + 1/*begin dummy*/; j != i->second.end() - 1/*end dummy*/; ++j)
            genomePtrs.push_back(genome_entry_ptr(&(*(j - 1))/*prior*/, &(*j)/*main annot*/, &(*(j + 1)) /*next*/)); // insert addresses
    }
}

int main(int argc, const char *argv[])
{
    // todo: make vap_utils use git submodules

    std::set<std::string> filter;
    genome_dataset genome;
    std::vector<genome_entry_ptr> genome_ptrs;

    parameters params = parseArguments(argc, argv); // get command line arguments

    readFilter(params.filterfile, filter);

    readGenome(params.inputfile, genome);

    buildGenomePtrs(genome_ptrs, genome);

    write(params.outputfile, genome_ptrs, params, filter);

    return 0;
}
